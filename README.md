# Учет студентов

Приложение учета студентов, разработанное с использованием следующих технологий:
- Java 17
- Spring Boot
- Spring Data JPA
- PostgreSQL
- REST API
- Maven
- Lombok

## Установка и настройка

1. Клонируйте репозиторий на ваше устройство:

```bash
git clone git@gitlab.com:VladSemenovForVibeLab/api-student-rest-stream-example.git
```

2. Запустите приложение, используя команду:

```bash
mvn spring-boot:run
```

3. Приложение будет доступно по адресу [http://localhost:8080](http://localhost:8080).

## Использование

Приложение предоставляет следующие REST API эндпоинты:

- `GET /students` - получение списка всех студентов
- `GET /students/{email}` - получение информации о студенте с указанным email
- `POST /students/save_student` - создание нового студента
- `PUT students/update_student` - обновление информации о студенте 
- `DELETE /students/delete_student/{email}` - удаление студента с указанным email

## Технические требования

Для успешной работы данного приложения необходимо установить следующие компоненты:

- Java 17
- PostgreSQL
- Maven

## Зависимости проекта

Данный проект использует следующие зависимости:

- Spring Boot Starter Web
- Spring Boot Starter Data JPA
- PostgreSQL Driver
- Lombok

## Контакты

Если у вас возникли проблемы или вопросы по использованию данного приложения, свяжитесь со мной по адресу ooovladislavchik@gmail.com